﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataVaultProject.Models;

namespace DataVaultProject.Models
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options): base(options)
        {
            this.Database.EnsureCreated();
            try
            {
                //var databaseCreator = (Database.GetService<IDatabaseCreator>() as RelationalDatabaseCreator);
                //databaseCreator.CreateTables();
            }
            catch (System.Data.SqlClient.SqlException)
            {
                //exception if table already created - ignore error
            }

        }
     

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().ToTable("Customers");
            modelBuilder.Entity<Import>().ToTable("Imports");
            modelBuilder.Entity<Order>().ToTable("Orders");
            modelBuilder.Entity<Book>().ToTable("Books");
            modelBuilder.Entity<Bookstore>().ToTable("Bookstores");

        }

        //select * from dbo.Books a  left join dbo.BOOK_HUB b on a.Isbn13 = b.ISBN13 where b.ISBN13 is null
        public DbSet<Book> Books { get; set; }
        public DbSet<Bookstore> Bookstores { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Import> Imports { get; set; }
    }
}
