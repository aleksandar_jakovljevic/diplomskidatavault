﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataVaultProject.Models
{
    public class Import
    {
        [Key]
        public int ImportId { get; set; }
        public string Isbn13 { get; set; }
        public string Title { get; set; }
        public int BookstoreId { get; set; }
        [DisplayName("Number of books to import")]
        public int Count { get; set; }
        public int Available { get; set; }
        [DisplayName("Lending?")]
        public bool Lend { get; set; }
        [DisplayName("Selling?")]
        public bool Sell { get; set; }
        public float Price { get; set; }
    }
}
