﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataVaultProject.Models
{
    public class Order
    {
        [Key]
        public int OrderId { get; set; }
        public string Isbn13 { get; set; }
        [DisplayName("Title")]
        public string BookName { get; set; }
        [DisplayName("Customer Number")]
        public int CustomerId { get; set; }
        [DisplayName("Bookstore Name")]
        public int BookstoreId { get; set; }
        public string BookstoreName { get; set; }
        public Guid BookHkey { get; set; }
        public Guid BookItemHkey { get; set; }
        public decimal Price { get; set; }
        public int Status { get; set; }//0 - in progress, 1 - failed, 2 - succeed, on hold?
    }
}
