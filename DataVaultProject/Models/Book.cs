﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataVaultProject.Models
{
    public class Book
    {
        public string Title { get; set; }
        public string Authors { get; set; }
        public double AverageRating { get; set; }
        public string Isbn10 { get; set; }

        [Key]
        public string Isbn13 { get; set; }
        public string LanguageCode { get; set; }
        public int NumPages { get; set; }
        public int RatingsCount { get; set; }
        public int TextReviewCount { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime PublicationDate { get; set; }
        public string Publisher { get; set; }

    } 
}
