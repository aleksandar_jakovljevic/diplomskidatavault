﻿using DataVaultProject.Encription.HashDiff;
using DataVaultProject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


namespace DataVaultProject.Services
{
    public class BookService 
    {

		public void ImportBook(string isbn, string bookstoreId, IFormCollection collection)
		{
			
			string connectionString = Startup.ConnectionString;

			using (SqlConnection connection = new SqlConnection(connectionString))
			{

				SqlCommand commandBookHkey = new SqlCommand(" SELECT BOOK_HKEY FROM BOOK_HUB WHERE ISBN13 = @Isbn13", connection);
				commandBookHkey.CommandType = CommandType.Text;
				commandBookHkey.Parameters.AddWithValue("@Isbn13", isbn);

				SqlCommand commandBookstoreHkey = new SqlCommand(" SELECT BOOKSTORE_HKEY FROM BOOKSTORE_HUB WHERE BOOKSTORE_ID = @bookstoreId", connection);
				commandBookstoreHkey.CommandType = CommandType.Text;
				commandBookstoreHkey.Parameters.AddWithValue("@bookstoreId", bookstoreId);

				connection.Open();

				var bookHkey = commandBookHkey.ExecuteScalar();
				var bookstoreHkey = commandBookstoreHkey.ExecuteScalar();

				var bookCount = Int32.Parse(collection["Count"].ToString());

				

				//CALCULATE HASH FOR NON VIEW DATA
				IFormCollection collectionHash = new FormCollection(new Dictionary<string, Microsoft.Extensions.Primitives.StringValues>
				{
					{ "Count", bookCount.ToString() },
					{ "Available",bookCount.ToString() }
				});
				var hashDiffInfo = HashDiff.GetHashDiff(collectionHash);


				SqlCommand commandHub = new SqlCommand("INSERT INTO BOOK_INFO_SAT(BOOK_HKEY,LOAD_DATE,RECORD_SOURCE,HASH_DIFF,COUNT,AVAILABLE) VALUES(@bookHkey,getDate(),'Admin',@HashDiff, @Count,@Count)", connection);
				commandHub.CommandType = CommandType.Text;
				commandHub.Parameters.AddWithValue("@bookHkey", bookHkey);
				commandHub.Parameters.AddWithValue("@HashDiff", hashDiffInfo);
				commandHub.Parameters.AddWithValue("@Count", bookCount);

				System.Guid uniqueId = Guid.NewGuid();
				SqlCommand commandLink = new SqlCommand("INSERT INTO BOOK_BOOKSTORE_LINK(BOOK_BOOKSTORE_HKEY,LOAD_DATE,RECORD_SOURCE,BOOK_HKEY,BOOKSTORE_HKEY) VALUES(@uniqueId,getDate(),'Admin',@bookHkey, @bookstoreHkey)", connection);
				commandLink.CommandType = CommandType.Text;
				commandLink.Parameters.AddWithValue("@uniqueId", uniqueId);
				commandLink.Parameters.AddWithValue("@bookHkey", bookHkey);
				commandLink.Parameters.AddWithValue("@bookstoreHkey", bookstoreHkey);
			

				SqlCommand commandSat = new SqlCommand(string.Format(@"

		
		INSERT INTO BOOK_ITEM_SAT
			(BOOK_ITEM_HKEY,LOAD_DATE,RECORD_SOURCE,HASH_DIFF,BOOK_HKEY,STATUS,DUE_DATE)
		VALUES
			
			
			"), connection);

				List<Guid> uniqueIds = new List<Guid>();
				for (int i = 0;i<bookCount;i++)
				{
					if(i!=0)
					{
						commandSat.CommandText += ", ";
					}

					uniqueIds.Add(Guid.NewGuid());

					IFormCollection collectionItemHash = new FormCollection(new Dictionary<string, Microsoft.Extensions.Primitives.StringValues>
					{
						{ "Status", "1" },
						{ "DueDate", "" }
					});
						var hashDiffItem = HashDiff.GetHashDiff(collectionItemHash);

					commandSat.CommandText += "(@uniqueIds"+i+",getDate(),'Admin',@HashDiff"+i+", @BookHkey"+i+",1,null)";
					commandSat.CommandType = CommandType.Text;
					commandSat.Parameters.AddWithValue("@uniqueIds"+i, uniqueIds[i]);
					//commandSat.Parameters.AddWithValue("@RecordSource"+i, "Aleksandar");
					commandSat.Parameters.AddWithValue("@HashDiff" + i, hashDiffItem);
					commandSat.Parameters.AddWithValue("@BookHkey" + i, bookHkey);
					//commandSat.Parameters.AddWithValue("@Status" + i, 1);
					//commandSat.Parameters.AddWithValue("@DueDate" + i, DBNull.Value);
				}



				SqlTransaction sqlTran = connection.BeginTransaction();

				commandHub.Transaction = sqlTran;
				commandHub.ExecuteNonQuery();

				commandLink.Transaction = sqlTran;
				commandLink.ExecuteNonQuery();

				commandSat.Transaction = sqlTran;
				commandSat.ExecuteNonQuery();

				sqlTran.Commit();
			}
		}







		public List<Book> GetAllFilter(IFormCollection collection)
		{
			string isbn13 = collection["isbn13"] != "" ? collection["isbn13"].ToString() : null;
			string isbn10 = collection["isbn10"] != "" ? collection["isbn13"].ToString() : null;
			string title = collection["title"] != "" ? collection["title"].ToString() : null;
			string authors = collection["authors"] != "" ? collection["authors"].ToString() : null;
			string numPages = collection["numPages"] != "" ? collection["numPages"].ToString() : null;
			string publicationDate = collection["publicationDate"] != "" ? collection["publicationDate"].ToString() : null;
			string publisher = collection["publisher"] != "" ? collection["publisher"].ToString() : null;
		
			List<Book> booksList = new List<Book>();
			string connectionString = Startup.ConnectionString; 

			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				SqlCommand cmd = new SqlCommand();
				cmd.Connection = connection;

				string command = "SELECT * FROM BOOK_HUB hub inner join BOOK_SAT_LATEST sat on hub.BOOK_HKEY = sat.BOOK_HKEY WHERE 1=1";

				if (isbn13 != null)
				{
					command = command + " and hub.ISBN13 = @isbn13";
					cmd.Parameters.AddWithValue("@isbn13", isbn13);
				}
				if (isbn10 != null)
				{
					command = command + " and sat.ISBN10 = @isbn10";
					cmd.Parameters.AddWithValue("@isbn10", isbn10);
				}
				if (title != null)
				{
					command = command + " and sat.TITLE LIKE @title"; 
					cmd.Parameters.AddWithValue("@title", title + "%");
				}

				if (authors != null)
				{
					command = command + " and sat.AUTHORS LIKE @authors";
					cmd.Parameters.AddWithValue("@authors", authors + "%");
				}

				if (numPages != null)
				{
					command = command + " and sat.NUMBER_PAGES = @numPages";
					cmd.Parameters.AddWithValue("@numPages", numPages);
				}

				if (publicationDate != null)
				{
					command = command + " and sat.PUBLICATION_DATE = @publicationDate";
					cmd.Parameters.AddWithValue("@publicationDate", publicationDate);
				}

				if (publisher != null)
				{
					command = command + " and sat.PUBLISHER LIKE @publisher";
					cmd.Parameters.AddWithValue("@publisher", publisher + "%");
				}

				cmd.CommandText = command;

				cmd.CommandType = CommandType.Text;
				connection.Open();

				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					var book = new Book();

					//book.BookId = Convert.ToInt32(rdr["bookId"]);
					book.Title = rdr["title"].ToString();
					book.Authors = rdr["authors"].ToString();
					//book.AverageRating = Convert.ToDouble(rdr["average_rating"]);
					book.Isbn10 = rdr["isbn10"].ToString();
					book.Isbn13 = rdr["isbn13"].ToString();
					book.LanguageCode = rdr["language_code"].ToString();
					book.NumPages = Convert.ToInt32(rdr["number_pages"]);
					//book.RatingsCount = Convert.ToInt32(rdr["ratings_count"]);
					//book.TextReviewCount = Convert.ToInt32(rdr["text_reviews_count"]);
					book.PublicationDate = Convert.ToDateTime(rdr["publication_date"]);
					book.Publisher = rdr["publisher"].ToString();

					booksList.Add(book);
				}
			}
			return booksList;
		}

		public List<Book> GetAll(){

			List<Book> booksList = new List<Book>();
			string connectionString = Startup.ConnectionString; 
			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				SqlCommand cmd = new SqlCommand("SELECT top 100 * FROM BOOK_HUB hub inner join BOOK_SAT_LATEST sat on hub.BOOK_HKEY = sat.BOOK_HKEY", connection);
				cmd.CommandType = CommandType.Text;
				connection.Open();

				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					var book = new Book();

					//book.BookId = Convert.ToInt32(rdr["bookId"]);
					book.Title = rdr["title"].ToString();
					book.Authors = rdr["authors"].ToString();
					//book.AverageRating = Convert.ToDouble(rdr["average_rating"]);
					book.Isbn10 = rdr["isbn10"].ToString();
					book.Isbn13 = rdr["isbn13"].ToString();
					book.LanguageCode = rdr["language_code"].ToString();
					book.NumPages = Convert.ToInt32(rdr["number_pages"]);
					//book.RatingsCount = Convert.ToInt32(rdr["ratings_count"]);
					//book.TextReviewCount = Convert.ToInt32(rdr["text_reviews_count"]);
					book.PublicationDate = rdr["publication_date"] != DBNull.Value ? Convert.ToDateTime(rdr["publication_date"]) : DateTime.MinValue;
					book.Publisher = rdr["publisher"].ToString();

					booksList.Add(book);
				}
			}
			return booksList;
        }

		public Book GetBook(string isbn)
		{
			Book book = new Book();
			string connectionString = Startup.ConnectionString;

			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				SqlCommand cmd = new SqlCommand("SELECT * FROM BOOK_HUB hub inner join BOOK_SAT_LATEST sat on hub.BOOK_HKEY = sat.BOOK_HKEY WHERE hub.ISBN13 = @isbn13", connection);
				cmd.Parameters.AddWithValue("@isbn13", isbn);
				cmd.CommandType = CommandType.Text;
				connection.Open();

				//Select* from book_hub hub
				//inner join BOOK_SAT sat on hub.BOOK_HKEY = sat.BOOK_HKEY
				//where sat.TITLE LIKE 'The lord%'

				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					//book.BookId = Convert.ToInt32(rdr["bookId"]);
					book.Title = rdr["title"].ToString();
					book.Authors = rdr["authors"].ToString();
					//book.AverageRating = Convert.ToDouble(rdr["average_rating"]);
					book.Isbn10 = rdr["isbn10"].ToString(); 
					book.Isbn13 = rdr["isbn13"].ToString();
					book.LanguageCode = rdr["language_code"].ToString();
					book.NumPages = Convert.ToInt32(rdr["number_pages"]);
					//book.RatingsCount = Convert.ToInt32(rdr["ratings_count"]);
					//book.TextReviewCount = Convert.ToInt32(rdr["text_reviews_count"]);
					book.PublicationDate = rdr["publication_date"] != DBNull.Value ? Convert.ToDateTime(rdr["publication_date"]) : DateTime.MinValue;
					book.Publisher = rdr["publisher"].ToString();
				}
			}

			return book;
		}
		public void AddBook(Book book)
		{
			var hashDiff = HashDiff.GetHashDiff(book);
			string connectionString = Startup.ConnectionString;

			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				System.Guid uniqueId = Guid.NewGuid();

				SqlCommand commandHub = new SqlCommand("INSERT INTO BOOK_HUB(BOOK_HKEY,LOAD_DATE,RECORD_SOURCE,ISBN13) VALUES(@uniqueId,getDate(),'Admin',@Isbn13)", connection);
				commandHub.CommandType = CommandType.Text;
				commandHub.Parameters.AddWithValue("@uniqueId", uniqueId);
				commandHub.Parameters.AddWithValue("@Isbn13", book.Isbn13);


				SqlCommand commandSat = new SqlCommand(string.Format(@"
		
		INSERT INTO BOOK_SAT
			(BOOK_HKEY,LOAD_DATE,RECORD_SOURCE,HASH_DIFF,TITLE,AUTHORS,ISBN10,LANGUAGE_CODE,NUMBER_PAGES,PUBLICATION_DATE,PUBLISHER)
		VALUES
			(@uniqueId,getDate(), @RecordSource,@HashDiff, @Title,@Authors,@Isbn10,@LanguageCode, @NumberPages, @publicationDate, @Publisher)
			
			"), connection);

				commandSat.CommandType = CommandType.Text;
				commandSat.Parameters.AddWithValue("@uniqueId", uniqueId);
				commandSat.Parameters.AddWithValue("@RecordSource", "Aleksandar");
				commandSat.Parameters.AddWithValue("@HashDiff", hashDiff);

				commandSat.Parameters.AddWithValue("@Title", book.Title);
				commandSat.Parameters.AddWithValue("@Authors", book.Authors);
				commandSat.Parameters.AddWithValue("@Isbn10", book.Isbn10);

				commandSat.Parameters.AddWithValue("@LanguageCode", book.LanguageCode);
				commandSat.Parameters.AddWithValue("@NumberPages", book.NumPages);

				commandSat.Parameters.AddWithValue("@publicationDate", book.PublicationDate.ToString() != "" ? (object)(book.PublicationDate) : DBNull.Value);
				commandSat.Parameters.AddWithValue("@Publisher", book.Publisher);

				connection.Open();
				SqlTransaction sqlTran = connection.BeginTransaction();

				commandHub.Transaction = sqlTran;
				commandHub.ExecuteNonQuery();

				commandSat.Transaction = sqlTran;
				var result = commandSat.ExecuteNonQuery();

				sqlTran.Commit();
			}
		}

		public void AddBook(IFormCollection collection)
		{
			var hashDiff = HashDiff.GetHashDiff(collection);
			string connectionString = Startup.ConnectionString;

			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				System.Guid uniqueId = Guid.NewGuid();

				SqlCommand commandHub = new SqlCommand("INSERT INTO BOOK_HUB(BOOK_HKEY,LOAD_DATE,RECORD_SOURCE,ISBN13) VALUES(@uniqueId,getDate(),'Admin',@Isbn13)", connection);
				commandHub.CommandType = CommandType.Text;
				commandHub.Parameters.AddWithValue("@uniqueId", uniqueId);
				commandHub.Parameters.AddWithValue("@Isbn13", collection["Isbn13"].ToString());


				SqlCommand commandSat = new SqlCommand(string.Format(@"
		
		INSERT INTO BOOK_SAT
			(BOOK_HKEY,LOAD_DATE,RECORD_SOURCE,HASH_DIFF,TITLE,AUTHORS,ISBN10,LANGUAGE_CODE,NUMBER_PAGES,PUBLICATION_DATE,PUBLISHER)
		VALUES
			(@uniqueId,getDate(), @RecordSource,@HashDiff, @Title,@Authors,@Isbn10,@LanguageCode, @NumberPages, @publicationDate, @Publisher)
			
			"), connection);

				commandSat.CommandType = CommandType.Text;
				commandSat.Parameters.AddWithValue("@uniqueId", uniqueId);
				commandSat.Parameters.AddWithValue("@RecordSource", "Aleksandar");
				commandSat.Parameters.AddWithValue("@HashDiff", hashDiff);

				commandSat.Parameters.AddWithValue("@Title", collection["Title"].ToString());
				commandSat.Parameters.AddWithValue("@Authors", collection["Authors"].ToString());
				commandSat.Parameters.AddWithValue("@Isbn10", collection["Isbn10"].ToString());

				commandSat.Parameters.AddWithValue("@LanguageCode", collection["LanguageCode"].ToString());
				commandSat.Parameters.AddWithValue("@NumberPages", collection["NumPages"].ToString());
				
				commandSat.Parameters.AddWithValue("@publicationDate", collection["PublicationDate"].ToString() != "" ? (object)DateTime.Parse(collection["PublicationDate"]) : DBNull.Value);
				commandSat.Parameters.AddWithValue("@Publisher", collection["Publisher"].ToString());
			
				connection.Open();
				SqlTransaction sqlTran = connection.BeginTransaction();

				commandHub.Transaction = sqlTran;
				commandHub.ExecuteNonQuery();

				commandSat.Transaction = sqlTran;
				var result = commandSat.ExecuteNonQuery();

				sqlTran.Commit();
			}
		}

	
		public void UpdateBook(string isbn, IFormCollection collection)
		{
			var hashDiff = HashDiff.GetHashDiff(collection);

			string connectionString = Startup.ConnectionString;
			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				//System.Guid uniqueId = Guid.NewGuid();

				SqlCommand commandHub = new SqlCommand("SELECT BOOK_HKEY FROM BOOK_HUB WHERE ISBN13 = @Isbn13", connection);
				commandHub.CommandType = CommandType.Text;
				commandHub.Parameters.AddWithValue("@Isbn13", isbn);

				connection.Open();
				
				var bookHkey = commandHub.ExecuteScalar();

				SqlTransaction sqlTran = connection.BeginTransaction();

				if (bookHkey != null)
				{

					SqlCommand commandSat = new SqlCommand(string.Format(@"

		 
		IF NOT EXISTS(SELECT 1 FROM BOOK_SAT WHERE HASH_DIFF = @HashDiff)
		INSERT INTO BOOK_SAT
			(BOOK_HKEY,LOAD_DATE,RECORD_SOURCE, HASH_DIFF, TITLE,AUTHORS,ISBN10,LANGUAGE_CODE,NUMBER_PAGES,PUBLICATION_DATE,PUBLISHER) 
		VALUES
			(@uniqueId,getDate(), @RecordSource,@HashDiff,@Title,@Authors,@Isbn10,@LanguageCode, @NumberPages, @PublicationDate, @Publisher)
		
				"), connection);

					commandSat.CommandType = CommandType.Text;
					commandSat.Parameters.AddWithValue("@uniqueId", Guid.Parse(bookHkey.ToString()));
					commandSat.Parameters.AddWithValue("@RecordSource", "Aleksandar");
					commandSat.Parameters.AddWithValue("@HashDiff", Guid.Parse(hashDiff));
					commandSat.Parameters.AddWithValue("@Title", collection["Title"].ToString());
					commandSat.Parameters.AddWithValue("@Authors", collection["Authors"].ToString());
					commandSat.Parameters.AddWithValue("@Isbn10", collection["Isbn10"].ToString());

					commandSat.Parameters.AddWithValue("@LanguageCode", collection["LanguageCode"].ToString());
					commandSat.Parameters.AddWithValue("@NumberPages", (object)collection["NumPages"].ToString());
					commandSat.Parameters.AddWithValue("@publicationDate", collection["PublicationDate"].ToString() != "" ? (object)DateTime.Parse(collection["PublicationDate"]) : DBNull.Value);
					commandSat.Parameters.AddWithValue("@Publisher", collection["Publisher"].ToString());

					commandSat.Transaction = sqlTran;
					commandSat.ExecuteNonQuery();
				}

				sqlTran.Commit();
			}
		}



		//public void AddBookAsync(IFormCollection collection)
		//{
		//	string connectionString = Startup.ConnectionString;
		//	using (SqlConnection connection = new SqlConnection(connectionString))
		//	{
		//		System.Guid uniqueId = Guid.NewGuid();

		//		//SqlCommand commandHub = new SqlCommand("USE TEST INSERT INTO BOOK_HUB(BOOK_HKEY,LOAD_DATE,RECORD_SOURCE,BOOK_ID) VALUES(@uniqueId,getDate(),'Admin',@Isbn13)", connection);
		//		//commandHub.CommandType = CommandType.Text;
		//		//commandHub.Parameters.AddWithValue("@uniqueId", uniqueId);
		//		//commandHub.Parameters.AddWithValue("@Isbn13", collection["Isbn13"].ToString());


		//		//SqlCommand commandSat = new SqlCommand("USE TEST INSERT INTO BOOK_SAT(BOOK_HKEY,LOAD_DATE,RECORD_SOURCE,TITLE,AUTHORS,ISBN,ISBN13,LANGUAGE_CODE,NUMBER_PAGES,PUBLICATION_DATE,PUBLISHER) VALUES(@uniqueId,getDate(), @RecordSource,@Title,@Authors,@Isbn10,@LanguageCode, @NumberPages, '2020-03-11', @Publisher)", connection);
		//		//commandSat.CommandType = CommandType.Text;
		//		//commandSat.Parameters.AddWithValue("@uniqueId", uniqueId);
		//		//commandSat.Parameters.AddWithValue("@RecordSource", "Aleksandar");
		//		//commandSat.Parameters.AddWithValue("@Title", collection["Title"].ToString());
		//		//commandSat.Parameters.AddWithValue("@Authors", collection["Authors"].ToString());
		//		//commandSat.Parameters.AddWithValue("@Isbn10", collection["Isbn"].ToString());

		//		//commandSat.Parameters.AddWithValue("@LanguageCode", collection["LanguageCode"].ToString());
		//		//commandSat.Parameters.AddWithValue("@NumberPages", Int32.Parse(collection["NumPages"].ToString()));
		//		////commandSat.Parameters.AddWithValue("@publicationDate", collection["publicationDate"]);
		//		//commandSat.Parameters.AddWithValue("@Publisher", collection["Publisher"].ToString());

		//		connection.Open();
		//		SqlTransaction sqlTran = connection.BeginTransaction();

		//		Task<int> task1 = InsertBookHubAsync(collection, uniqueId, sqlTran, connection);
		//		Task<int> task2 = InsertBookSatAsync(collection, uniqueId, sqlTran, connection);

		//		Task.WaitAll(new Task[] { task1, task2 }); //synchronously wait
		//		//int[] results = await Task.WhenAll(new Task<int>[] { task1, task2 });

		//		sqlTran.Commit();
		//	}
		//}


		//public async Task<int> InsertBookHubAsync(IFormCollection collection, Guid uniqueId, SqlTransaction sqlTran, SqlConnection connection)
		//{

		//	SqlCommand commandHub = new SqlCommand("USE TEST INSERT INTO BOOK_HUB(BOOK_HKEY,LOAD_DATE,RECORD_SOURCE,BOOK_ID) VALUES(@uniqueId,getDate(),'Admin',@Isbn13)", connection);
		//	commandHub.CommandType = CommandType.Text;
		//	commandHub.Parameters.AddWithValue("@uniqueId", uniqueId);
		//	commandHub.Parameters.AddWithValue("@Isbn13", collection["Isbn13"].ToString());

		//	commandHub.Transaction = sqlTran;
		//	return await commandHub.ExecuteNonQueryAsync();

		//}


		//public async Task<int> InsertBookSatAsync(IFormCollection collection, Guid uniqueId, SqlTransaction sqlTran, SqlConnection connection)
		//{

		//	SqlCommand commandSat = new SqlCommand("USE TEST INSERT INTO BOOK_SAT(BOOK_HKEY,LOAD_DATE,RECORD_SOURCE,TITLE,AUTHORS,ISBN,ISBN13,LANGUAGE_CODE,NUMBER_PAGES,PUBLICATION_DATE,PUBLISHER) VALUES(@uniqueId,getDate(), @RecordSource,@Title,@Authors,@Isbn10,@LanguageCode, @NumberPages, '2020-03-11', @Publisher)", connection);
		//	commandSat.CommandType = CommandType.Text;
		//	commandSat.Parameters.AddWithValue("@uniqueId", uniqueId);
		//	commandSat.Parameters.AddWithValue("@RecordSource", "Aleksandar");
		//	commandSat.Parameters.AddWithValue("@Title", collection["Title"].ToString());
		//	commandSat.Parameters.AddWithValue("@Authors", collection["Authors"].ToString());
		//	commandSat.Parameters.AddWithValue("@Isbn10", collection["Isbn"].ToString());

		//	commandSat.Parameters.AddWithValue("@LanguageCode", collection["LanguageCode"].ToString());
		//	commandSat.Parameters.AddWithValue("@NumberPages", Int32.Parse(collection["NumPages"].ToString()));
		//	//commandSat.Parameters.AddWithValue("@publicationDate", collection["publicationDate"]);
		//	commandSat.Parameters.AddWithValue("@Publisher", collection["Publisher"].ToString());

		//	commandSat.Transaction = sqlTran;
		//	return await commandSat.ExecuteNonQueryAsync();

		//}



	}
}
