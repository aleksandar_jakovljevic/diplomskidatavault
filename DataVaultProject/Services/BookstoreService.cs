﻿using DataVaultProject.Encription.HashDiff;
using DataVaultProject.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace DataVaultProject.Services
{
    public class BookstoreService
    {


		//
		public string GetBookIfAvailable(string isbn13, string bookstoreId, out string bookItemHkey, out string price)
		{
			string bookHkey = "";
			string connectionString = Startup.ConnectionString;
			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				
				SqlCommand cmd = new SqlCommand(string.Format(@"

	            SELECT l.BOOK_HKEY, i.BOOK_ITEM_HKEY, i.PRICE from dbo.BOOKSTORE_BOOK_LINK l
					join dbo.BOOKSTORE_BOOK_LINK_SAT s on s.BOOKSTORE_BOOK_LINK_HKEY = l.BOOKSTORE_BOOK_LINK_HKEY
					join
						(select top 1 * 
						from dbo.BOOKSTORE_BOOK_ITEM_SAT i 
						where i.STATUS = 1
						) i on i.BOOKSTORE_BOOK_LINK_HKEY = l.BOOKSTORE_BOOK_LINK_HKEY
				where l.ISBN13 = @isbn13
						and l.BOOKSTORE_ID = @bookstoreId
						and s.AVAILABLE > 0

				"), connection);

				cmd.CommandType = CommandType.Text;
				cmd.Parameters.AddWithValue("@isbn13", isbn13);
				cmd.Parameters.AddWithValue("@bookstoreId", bookstoreId);

				connection.Open();

				SqlDataReader rdr = cmd.ExecuteReader();

				bookItemHkey = "";
				price = "";

				while (rdr.Read())
				{
					bookHkey = rdr["BOOK_HKEY"].ToString();
					bookItemHkey = rdr["BOOK_ITEM_HKEY"].ToString();
					price = rdr["PRICE"].ToString();
				}
			}

			return bookHkey;
		}




		//TODO elasticsearch bookstore
		public Dictionary<int,string> getAllBookstores()
		{
			Dictionary<int, string> bookstores = new Dictionary<int, string>();

			string connectionString = Startup.ConnectionString;
			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				SqlCommand cmd = new SqlCommand("SELECT hub.BOOKSTORE_ID, sat.NAME FROM BOOKSTORE_HUB hub inner join BOOKSTORE_SAT_LATEST sat on hub.BOOKSTORE_HKEY = sat.BOOKSTORE_HKEY", connection);
				cmd.CommandType = CommandType.Text;
				connection.Open();

				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					bookstores.Add(Convert.ToInt32(rdr["BOOKSTORE_ID"]), rdr["NAME"].ToString());
				}
			}

			return bookstores;
		}







		public List<Bookstore> GetAll()
		{

			List<Bookstore> bookstoreList = new List<Bookstore>();
			string connectionString = Startup.ConnectionString;
			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				SqlCommand cmd = new SqlCommand("SELECT top 100 * FROM BOOKSTORE_HUB hub inner join BOOKSTORE_SAT_LATEST sat on hub.BOOKSTORE_HKEY = sat.BOOKSTORE_HKEY", connection);
				cmd.CommandType = CommandType.Text;
				connection.Open();

				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					var bookstore = new Bookstore();

					bookstore.BookstoreId = Convert.ToInt32(rdr["BOOKSTORE_ID"]);
					bookstore.Name = rdr["NAME"].ToString();
					bookstore.Street = rdr["STREET_NAME"].ToString();
					bookstore.City = rdr["CITY"].ToString();
					bookstore.Country = rdr["COUNTRY"].ToString();
					bookstore.PostalCode =rdr["POSTAL_CODE"].ToString();
					bookstore.BankAccount = rdr["BANK_ACCOUNT"].ToString();


					bookstoreList.Add(bookstore);
				}
			}
			return bookstoreList;
		}

		public Bookstore GetBookstore(int bookstoreId)
		{
			Bookstore bookstore = new Bookstore();
			string connectionString = Startup.ConnectionString;

			using (SqlConnection connection = new SqlConnection(connectionString))
			{

				SqlCommand cmd = new SqlCommand(string.Format(@"

		
		SELECT * FROM BOOKSTORE_HUB hub 
			inner join BOOKSTORE_SAT sat on hub.BOOKSTORE_HKEY = sat.BOOKSTORE_HKEY 
		WHERE hub.BOOKSTORE_ID = @bookstoreId"

			), connection);

				cmd.Parameters.AddWithValue("@bookstoreId", bookstoreId);
				cmd.CommandType = CommandType.Text;
				connection.Open();

				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					bookstore.BookstoreId = Convert.ToInt32(rdr["BOOKSTORE_ID"]);
					bookstore.Name = rdr["NAME"].ToString();
					bookstore.Street = rdr["STREET_NAME"].ToString();
					bookstore.City = rdr["CITY"].ToString();
					bookstore.Country = rdr["COUNTRY"].ToString();
					bookstore.PostalCode = rdr["POSTAL_CODE"].ToString();
					bookstore.BankAccount = rdr["BANK_ACCOUNT"].ToString();
				}
			}

			return bookstore;
		}

		public void AddBookstore(IFormCollection collection)
		{
			var hashDiff = HashDiff.GetHashDiff(collection);
			string connectionString = Startup.ConnectionString;

			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				System.Guid uniqueId = Guid.NewGuid();

				SqlCommand commandHub = new SqlCommand(string.Format(@"

		
		INSERT INTO BOOKSTORE_HUB
			(BOOKSTORE_HKEY,LOAD_DATE,RECORD_SOURCE) 
		VALUES
			(@uniqueId,getDate(),@RecordSource)"

			), connection);

				commandHub.CommandType = CommandType.Text;
				commandHub.Parameters.AddWithValue("@uniqueId", uniqueId);
				commandHub.Parameters.AddWithValue("@RecordSource", "Admin");

				
				SqlCommand commandSat = new SqlCommand(string.Format(@"

		
		INSERT INTO BOOKSTORE_SAT
			(BOOKSTORE_HKEY,LOAD_DATE,RECORD_SOURCE,HASH_DIFF,NAME,STREET_NAME,CITY, COUNTRY,POSTAL_CODE,BANK_ACCOUNT) 
		VALUES
			(@uniqueId,getDate(), @RecordSource,@HashDiff,@Name,@Street,@City,@Country, @PostalCode, @BankAccount)"

			), connection);

				commandSat.CommandType = CommandType.Text;
				commandSat.Parameters.AddWithValue("@uniqueId", uniqueId);
				commandSat.Parameters.AddWithValue("@RecordSource", "Admin");
				commandSat.Parameters.AddWithValue("@HashDiff", hashDiff);
				commandSat.Parameters.AddWithValue("@Name", collection["Name"].ToString());
				commandSat.Parameters.AddWithValue("@Street", collection["Street"].ToString());
				commandSat.Parameters.AddWithValue("@City", collection["City"].ToString());
				commandSat.Parameters.AddWithValue("@Country", collection["Country"].ToString());
				commandSat.Parameters.AddWithValue("@PostalCode", collection["PostalCode"].ToString());
				commandSat.Parameters.AddWithValue("@BankAccount", collection["BankAccount"].ToString());

				connection.Open();
				SqlTransaction sqlTran = connection.BeginTransaction();

				commandHub.Transaction = sqlTran;
				commandHub.ExecuteNonQuery();

				commandSat.Transaction = sqlTran;
				var result = commandSat.ExecuteNonQuery();

				sqlTran.Commit();
			}
		}


		public void AddBookstore(Bookstore bookstore)
		{
			var hashDiff = HashDiff.GetHashDiff(bookstore);
			string connectionString = Startup.ConnectionString;

			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				System.Guid uniqueId = Guid.NewGuid();

				SqlCommand commandHub = new SqlCommand(string.Format(@"

		
		INSERT INTO BOOKSTORE_HUB
			(BOOKSTORE_HKEY,LOAD_DATE,RECORD_SOURCE) 
		VALUES
			(@uniqueId,getDate(),@RecordSource)"

			), connection);

				commandHub.CommandType = CommandType.Text;
				commandHub.Parameters.AddWithValue("@uniqueId", uniqueId);
				commandHub.Parameters.AddWithValue("@RecordSource", "Admin");


				SqlCommand commandSat = new SqlCommand(string.Format(@"

		
		INSERT INTO BOOKSTORE_SAT
			(BOOKSTORE_HKEY,LOAD_DATE,RECORD_SOURCE,HASH_DIFF,NAME,STREET_NAME,CITY, COUNTRY,POSTAL_CODE,BANK_ACCOUNT) 
		VALUES
			(@uniqueId,getDate(), @RecordSource,@HashDiff,@Name,@Street,@City,@Country, @PostalCode, @BankAccount)"

			), connection);

				commandSat.CommandType = CommandType.Text;
				commandSat.Parameters.AddWithValue("@uniqueId", uniqueId);
				commandSat.Parameters.AddWithValue("@RecordSource", "Admin");
				commandSat.Parameters.AddWithValue("@HashDiff", hashDiff);
				commandSat.Parameters.AddWithValue("@Name", bookstore.Name);
				commandSat.Parameters.AddWithValue("@Street", bookstore.Street);
				commandSat.Parameters.AddWithValue("@City", bookstore.City);
				commandSat.Parameters.AddWithValue("@Country", bookstore.Country);
				commandSat.Parameters.AddWithValue("@PostalCode", bookstore.PostalCode);
				commandSat.Parameters.AddWithValue("@BankAccount", bookstore.BankAccount);

				connection.Open();
				SqlTransaction sqlTran = connection.BeginTransaction();

				commandHub.Transaction = sqlTran;
				commandHub.ExecuteNonQuery();

				commandSat.Transaction = sqlTran;
				var result = commandSat.ExecuteNonQuery();

				sqlTran.Commit();
			}
		}

 

        public void UpdateBookstore(int bookstoreId, IFormCollection collection)
		{
			var hashDiff = HashDiff.GetHashDiff(collection);
			string connectionString = Startup.ConnectionString;

			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				//System.Guid uniqueId = Guid.NewGuid();

				SqlCommand commandHub = new SqlCommand("SELECT BOOKSTORE_HKEY FROM BOOKSTORE_HUB WHERE BOOKSTORE_ID = @BookstoreId", connection);
				commandHub.CommandType = CommandType.Text;
				commandHub.Parameters.AddWithValue("@BookstoreId", bookstoreId);

				connection.Open();

				var bookstoreHkey = commandHub.ExecuteScalar();

				SqlTransaction sqlTran = connection.BeginTransaction();

				if (bookstoreHkey != null)
				{

					SqlCommand commandSat = new SqlCommand(string.Format(@"

		
		IF NOT EXISTS(SELECT 1 FROM BOOKSTORE_SAT WHERE HASH_DIFF = @HashDiff)
		INSERT INTO BOOKSTORE_SAT
			(BOOKSTORE_HKEY,LOAD_DATE,RECORD_SOURCE, HASH_DIFF, NAME,STREET_NAME,CITY, COUNTRY,POSTAL_CODE,BANK_ACCOUNT) 
		VALUES
			(@uniqueId,getDate(), @RecordSource, @HashDiff, @Name,@Street,@City,@Country, @PostalCode, @BankAccount)"

				), connection);

					commandSat.CommandType = CommandType.Text;
					commandSat.Parameters.AddWithValue("@uniqueId", Guid.Parse(bookstoreHkey.ToString()));
					commandSat.Parameters.AddWithValue("@RecordSource", "Administrator");
					commandSat.Parameters.AddWithValue("@HashDiff", Guid.Parse(hashDiff));
					commandSat.Parameters.AddWithValue("@Name", collection["Name"].ToString());
					commandSat.Parameters.AddWithValue("@Street", collection["Street"].ToString());
					commandSat.Parameters.AddWithValue("@City", collection["City"].ToString());
					commandSat.Parameters.AddWithValue("@Country", collection["Country"].ToString());
					commandSat.Parameters.AddWithValue("@PostalCode", collection["PostalCode"].ToString());
					commandSat.Parameters.AddWithValue("@BankAccount", collection["BankAccount"].ToString());

					commandSat.Transaction = sqlTran;
					commandSat.ExecuteNonQuery();
				}

				sqlTran.Commit();
			}
		}
	}
}
