﻿using DataVaultProject.Encription.HashDiff;
using DataVaultProject.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace DataVaultProject.Services
{
    public class CustomerService
    {
		
		public List<Customer> GetAll()
		{

			List<Customer> bookstoreList = new List<Customer>();
			string connectionString = Startup.ConnectionString;
			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				SqlCommand cmd = new SqlCommand(string.Format(@"

				SELECT top 100 * 
			FROM CUSTOMER_HUB hub
				inner join CUSTOMER_SAT_LATEST sat on hub.CUSTOMER_HKEY = sat.CUSTOMER_HKEY 
				inner join CUSTOMER_BOOKSTORE_LINK link on link.CUSTOMER_HKEY = hub.CUSTOMER_HKEY
				inner join BOOKSTORE_HUB bHub on bHub.BOOKSTORE_HKEY = link.BOOKSTORE_HKEY

				"), connection);
				cmd.CommandType = CommandType.Text;
				connection.Open();

				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					var customer = new Customer();

					customer.CustomerId = Convert.ToInt32(rdr["CUSTOMER_ID"]);
					customer.Name = rdr["NAME"].ToString();
					customer.Surname = rdr["SURNAME"].ToString();
					customer.StreetName = rdr["STREET_NAME"].ToString();
					customer.StreetNumber = Convert.ToInt32(rdr["STREET_NUMBER"]);
					customer.City = rdr["CITY"].ToString();
					customer.Country = rdr["COUNTRY"].ToString();
					customer.PostalCode = rdr["POSTAL_CODE"].ToString();
					customer.BankAccount = rdr["BANK_ACCOUNT"].ToString();
					customer.DateOfBirth = rdr["DATE_OF_BIRTH"] != DBNull.Value ? Convert.ToDateTime(rdr["DATE_OF_BIRTH"]) : DateTime.MinValue;
					customer.Email = rdr["EMAIL"].ToString();
					customer.BookstoreId = Convert.ToInt32(rdr["BOOKSTORE_ID"]);

					bookstoreList.Add(customer);
				}
			}
			return bookstoreList;
		}


		public Customer GetCustomer(int customerId)
		{
			Customer customer = new Customer();
			string connectionString = Startup.ConnectionString;

			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				SqlCommand cmd = new SqlCommand(string.Format(@"

			SELECT * 
				FROM CUSTOMER_HUB hub 
					inner join CUSTOMER_SAT_LATEST sat on hub.CUSTOMER_HKEY = sat.CUSTOMER_HKEY 
					inner join CUSTOMER_BOOKSTORE_LINK link on link.CUSTOMER_HKEY = hub.CUSTOMER_HKEY
					inner join BOOKSTORE_HUB bHub on bHub.BOOKSTORE_HKEY = link.BOOKSTORE_HKEY
				WHERE hub.CUSTOMER_ID = @customerId

			"), connection);

				cmd.Parameters.AddWithValue("@customerId", customerId);
				cmd.CommandType = CommandType.Text;
				connection.Open();

				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					customer.CustomerId = Convert.ToInt32(rdr["CUSTOMER_ID"]);
					customer.Name = rdr["NAME"].ToString();
					customer.Surname = rdr["SURNAME"].ToString();
					customer.StreetName = rdr["STREET_NAME"].ToString();
					customer.StreetNumber = Convert.ToInt32(rdr["STREET_NUMBER"]);
					customer.City = rdr["CITY"].ToString();
					customer.Country = rdr["COUNTRY"].ToString();
					customer.PostalCode = rdr["POSTAL_CODE"].ToString();
					customer.BankAccount = rdr["BANK_ACCOUNT"].ToString();
					customer.DateOfBirth = rdr["DATE_OF_BIRTH"] != DBNull.Value ? Convert.ToDateTime(rdr["DATE_OF_BIRTH"]) : DateTime.MinValue;
					customer.Email = rdr["EMAIL"].ToString();
					customer.BookstoreId = Convert.ToInt32(rdr["BOOKSTORE_ID"]);
				}
			}

			return customer;
		}

		public void AddCustomer(string bookstoreId, IFormCollection collection)
		{
			var hashDiff = HashDiff.GetHashDiff(collection);
			string connectionString = Startup.ConnectionString;

			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				System.Guid uniqueId = Guid.NewGuid();

				SqlCommand commandHub = new SqlCommand("INSERT INTO CUSTOMER_HUB(CUSTOMER_HKEY,LOAD_DATE,RECORD_SOURCE) VALUES(@uniqueId,getDate(),@RecordSource)", connection);
				commandHub.CommandType = CommandType.Text;
				commandHub.Parameters.AddWithValue("@uniqueId", uniqueId);
				commandHub.Parameters.AddWithValue("@RecordSource", "Admin");

				SqlCommand commandSat = new SqlCommand(string.Format(@"

		
		INSERT INTO CUSTOMER_SAT
			(CUSTOMER_HKEY,LOAD_DATE,RECORD_SOURCE,HASH_DIFF,NAME,SURNAME,STREET_NAME, STREET_NUMBER, CITY, COUNTRY,POSTAL_CODE, DATE_OF_BIRTH ,BANK_ACCOUNT,EMAIL)
		VALUES
			(@uniqueId,getDate(), @RecordSource,@HashDiff, @Name,@Surname,@StreetName,@StreetNumber, @City,@Country, @PostalCode,@DateOfBirth, @BankAccount,@Email)

			"), connection);

				commandSat.CommandType = CommandType.Text;
				commandSat.Parameters.AddWithValue("@uniqueId", uniqueId);
				commandSat.Parameters.AddWithValue("@RecordSource", "Admin");
				commandSat.Parameters.AddWithValue("@HashDiff", hashDiff);
				commandSat.Parameters.AddWithValue("@Name", collection["Name"].ToString());
				commandSat.Parameters.AddWithValue("@Surname", collection["Surname"].ToString());
				commandSat.Parameters.AddWithValue("@StreetName", collection["StreetName"].ToString());
				commandSat.Parameters.AddWithValue("@StreetNumber", (object)(collection["StreetNumber"].ToString()));
				commandSat.Parameters.AddWithValue("@City", collection["City"].ToString());
				commandSat.Parameters.AddWithValue("@Country", collection["Country"].ToString());
				commandSat.Parameters.AddWithValue("@PostalCode", collection["PostalCode"].ToString());
				commandSat.Parameters.AddWithValue("@BankAccount", collection["BankAccount"].ToString());
				commandSat.Parameters.AddWithValue("@DateOfBirth", collection["DateOfBirth"].ToString() != "" ? (object)DateTime.Parse(collection["DateOfBirth"]) : DBNull.Value);
				commandSat.Parameters.AddWithValue("@Email", collection["Email"].ToString());

				connection.Open();
				SqlTransaction sqlTran = connection.BeginTransaction();

				commandHub.Transaction = sqlTran;
				commandHub.ExecuteNonQuery();

				commandSat.Transaction = sqlTran;
				var result = commandSat.ExecuteNonQuery();

				sqlTran.Commit();
			}
		}

	

		public void UpdateCustomer(int customerId, IFormCollection collection)
		{
			var hashDiff = HashDiff.GetHashDiff(collection);
			string connectionString = Startup.ConnectionString;

			using (SqlConnection connection = new SqlConnection(connectionString))
			{

				SqlCommand commandHub = new SqlCommand("SELECT CUSTOMER_HKEY FROM CUSTOMER_HUB WHERE CUSTOMER_ID = @CustomerId", connection);
				commandHub.CommandType = CommandType.Text;
				commandHub.Parameters.AddWithValue("@CustomerId", customerId);

				connection.Open();

				var customerHkey = commandHub.ExecuteScalar();

				SqlTransaction sqlTran = connection.BeginTransaction();

				if (customerHkey != null)
				{

					SqlCommand commandSat = new SqlCommand(string.Format(@"

			
			IF NOT EXISTS(SELECT 1 FROM CUSTOMER_SAT WHERE HASH_DIFF = @HashDiff)
			INSERT INTO CUSTOMER_SAT
				(CUSTOMER_HKEY,LOAD_DATE,RECORD_SOURCE, HASH_DIFF, NAME,SURNAME,STREET_NAME,STREET_NUMBER,CITY, COUNTRY,POSTAL_CODE,DATE_OF_BIRTH,BANK_ACCOUNT,EMAIL) 
			VALUES
				(@uniqueId,getDate(), @RecordSource,@HashDiff, @Name,@Surname,@StreetName,@StreetNumber,@City,@Country, @PostalCode,@DateOfBirth, @BankAccount,@Email)

				"), connection);

					commandSat.CommandType = CommandType.Text;
					commandSat.Parameters.AddWithValue("@uniqueId", customerHkey.ToString());
					commandSat.Parameters.AddWithValue("@RecordSource", "Admin");
					commandSat.Parameters.AddWithValue("@HashDiff", hashDiff);
					commandSat.Parameters.AddWithValue("@Name", collection["Name"].ToString());
					commandSat.Parameters.AddWithValue("@Surname", collection["Surname"].ToString());
					commandSat.Parameters.AddWithValue("@StreetName", collection["StreetName"].ToString());
					commandSat.Parameters.AddWithValue("@StreetNumber", (object)(collection["StreetNumber"].ToString()));
					commandSat.Parameters.AddWithValue("@City", collection["City"].ToString());
					commandSat.Parameters.AddWithValue("@Country", collection["Country"].ToString());
					commandSat.Parameters.AddWithValue("@PostalCode", collection["PostalCode"].ToString());
					commandSat.Parameters.AddWithValue("@BankAccount", collection["BankAccount"].ToString());
					commandSat.Parameters.AddWithValue("@DateOfBirth", collection["DateOfBirth"].ToString() != "" ? (object)DateTime.Parse(collection["DateOfBirth"]) : DBNull.Value);
					commandSat.Parameters.AddWithValue("@Email", collection["Email"].ToString());

					commandSat.Transaction = sqlTran;
					commandSat.ExecuteNonQuery();
				}

				sqlTran.Commit();
			}
		}


		//public void AddCustomerAsync(IFormCollection collection)
		//{
		//	string connectionString = Startup.ConnectionString;
		//	using (SqlConnection connection = new SqlConnection(connectionString))
		//	{
		//		System.Guid uniqueId = Guid.NewGuid();

		//		SqlCommand commandHub = new SqlCommand("USE TEST INSERT INTO CUSTOMER_HUB(CUSTOMER_HKEY,LOAD_DATE,RECORD_SOURCE,CUSTOMER_ID) VALUES(@uniqueId,getDate(),@RecordSource,@CustomerId)", connection);
		//		commandHub.CommandType = CommandType.Text;
		//		commandHub.Parameters.AddWithValue("@uniqueId", uniqueId);
		//		commandHub.Parameters.AddWithValue("@RecordSource", "Admin");
		//		commandHub.Parameters.AddWithValue("@CustomerId", Convert.ToInt32(collection["CustomerId"]));


		//		SqlCommand commandSat = new SqlCommand("USE TEST INSERT INTO CUSTOMER_SAT(CUSTOMER_HKEY,LOAD_DATE,RECORD_SOURCE,NAME,SURNAME,STREET_NAME,CITY, COUNTRY,POSTAL_CODE,BANK_ACCOUNT,EMAIL) VALUES(@uniqueId,getDate(), @RecordSource,@Name,@Surname,@StreetName,@City,@Country, @PostalCode, @BankAccount,@Email)", connection);
		//		commandSat.CommandType = CommandType.Text;
		//		commandSat.Parameters.AddWithValue("@uniqueId", uniqueId);
		//		commandSat.Parameters.AddWithValue("@RecordSource", "Admin");
		//		commandSat.Parameters.AddWithValue("@Name", collection["Name"].ToString());
		//		commandSat.Parameters.AddWithValue("@Surname", collection["Surname"].ToString());
		//		commandSat.Parameters.AddWithValue("@StreetName", collection["StreetName"].ToString());
		//		//commandSat.Parameters.AddWithValue("@StreetNumber", collection["StreetNumber"].ToString());
		//		commandSat.Parameters.AddWithValue("@City", collection["City"].ToString());
		//		commandSat.Parameters.AddWithValue("@Country", collection["Country"].ToString());
		//		commandSat.Parameters.AddWithValue("@PostalCode", collection["PostalCode"].ToString());
		//		commandSat.Parameters.AddWithValue("@BankAccount", collection["BankAccount"].ToString());
		//		//commandSat.Parameters.AddWithValue("@DateOfBirth", collection["DateOfBirth"].ToString());
		//		commandSat.Parameters.AddWithValue("@Email", collection["Email"].ToString());

		//		connection.Open();
		//		SqlTransaction sqlTran = connection.BeginTransaction();

		//		commandHub.Transaction = sqlTran;
		//		commandHub.ExecuteNonQuery();

		//		commandSat.Transaction = sqlTran;
		//		var result = commandSat.ExecuteNonQuery();

		//		sqlTran.Commit();
		//	}
		//}
	}
}
