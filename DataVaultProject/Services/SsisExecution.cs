﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.SqlServer.Dts.Runtime;
using Microsoft.SqlServer.Management.IntegrationServices;
using Microsoft.SqlServer.Management.Sdk.Sfc;
using Microsoft.SqlServer.Management.Smo;


using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo.Agent;

namespace DataVaultProject
{
    class MyEventListener : DefaultEvents
    {
        public override bool OnError(DtsObject source, int errorCode, string subComponent,
          string description, string helpFile, int helpContext, string idofInterfaceWithError)
        {
            // Add application-specific diagnostics here.  
            Console.WriteLine("Error in {0}/{1} : {2}", source, subComponent, description);
            return false;
        }
    }
    public class SsisExecution
    {
        public static void ExecuteBookstorePackage()
        {
        
            SqlConnection DbConn = new SqlConnection(Startup.ConnectionString);
            ServerConnection conn;
            Job job;
            Server server;

            using (DbConn)
            {
                server = new Server("LAPTOP-H1TDLTRQ\\SQL2019");
                server.ConnectionContext.Connect();
                job = server.JobServer.Jobs["SsisBookstoreJob"];
                if (job.CurrentRunStatus == JobExecutionStatus.Idle)
                    job.Start();
                while (job.CurrentRunStatus == JobExecutionStatus.Executing)
                {
                    job.Refresh();
                    Console.WriteLine($"Current status of {job.Name} is {job.CurrentRunStatus.ToString()}");
                    System.Threading.Thread.Sleep(1000);
                }
            }


        }

        public static void ExecuteCustomerPackage()
        {
            SqlConnection DbConn = new SqlConnection(Startup.ConnectionString);
            ServerConnection conn;
            Job job;
            Server server;

            using (DbConn)
            {
                server = new Server("LAPTOP-H1TDLTRQ\\SQL2019");
                server.ConnectionContext.Connect();
                job = server.JobServer.Jobs["SsisCustomerJob"];
                if (job.CurrentRunStatus == JobExecutionStatus.Idle)
                    job.Start();
                while (job.CurrentRunStatus == JobExecutionStatus.Executing)
                {
                    job.Refresh();
                    Console.WriteLine($"Current status of {job.Name} is {job.CurrentRunStatus.ToString()}");
                    System.Threading.Thread.Sleep(1000);
                }
            }


        }

        public static void ExecuteBookPackage()
        {         

            SqlConnection DbConn = new SqlConnection(Startup.ConnectionString);
            ServerConnection conn;
            Job job;
            Server server;

            using (DbConn)
            {
                server = new Server("LAPTOP-H1TDLTRQ\\SQL2019");
                server.ConnectionContext.Connect();
                job = server.JobServer.Jobs["SsisBookJob"];             
                if (job.CurrentRunStatus == JobExecutionStatus.Idle)
                    job.Start();
                while (job.CurrentRunStatus == JobExecutionStatus.Executing)
                {
                    job.Refresh();
                    Console.WriteLine($"Current status of {job.Name} is {job.CurrentRunStatus.ToString()}");
                    System.Threading.Thread.Sleep(1000);
                }
            }

        }


        public static void ExecuteImportPackage()
        {

            SqlConnection DbConn = new SqlConnection(Startup.ConnectionString);
            ServerConnection conn;
            Job job;
            Server server;

            using (DbConn)
            {
                server = new Server("LAPTOP-H1TDLTRQ\\SQL2019");
                server.ConnectionContext.Connect();
                job = server.JobServer.Jobs["SsisImportJob"];
                if (job.CurrentRunStatus == JobExecutionStatus.Idle)
                    job.Start();
                while (job.CurrentRunStatus == JobExecutionStatus.Executing)
                {
                    job.Refresh();
                    Console.WriteLine($"Current status of {job.Name} is {job.CurrentRunStatus.ToString()}");
                    System.Threading.Thread.Sleep(1000);
                }
            }

        }


        public static void ExecuteOrderPackage()
        {

            SqlConnection DbConn = new SqlConnection(Startup.ConnectionString);
            ServerConnection conn;
            Job job;
            Server server;

            using (DbConn)
            {
                server = new Server("LAPTOP-H1TDLTRQ\\SQL2019");
                server.ConnectionContext.Connect();
                job = server.JobServer.Jobs["SsisOrderJob"];
                if (job.CurrentRunStatus == JobExecutionStatus.Idle)
                    job.Start();
                while (job.CurrentRunStatus == JobExecutionStatus.Executing)
                {
                    job.Refresh();
                    Console.WriteLine($"Current status of {job.Name} is {job.CurrentRunStatus.ToString()}");
                    System.Threading.Thread.Sleep(1000);
                }
            }

        }
    }
}



