﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace DataVaultProject
{
    public static class DatabaseContext
    {
      
      //ADD ELASTIC SEARCH



     //BOOK PART
        public static void CreateBookTables()
        {
            string connectionString = Startup.ConnectionString;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                SqlCommand cmdBookHub = new SqlCommand(string.Format(@"

        if not exists (select * from sys.tables where name='BOOK_HUB')

        CREATE TABLE BOOK_HUB
        (BOOK_HKEY UNIQUEIDENTIFIER DEFAULT NEWID(), LOAD_DATE DATETIME2(7), RECORD_SOURCE nvarchar(32),ISBN13 nvarchar(13),
            CONSTRAINT BOOK_HUB_PK PRIMARY KEY (BOOK_HKEY))

            "), connection);

                SqlCommand cmdBookSat = new SqlCommand(string.Format(@"

        if not exists (select * from sys.tables where name='BOOK_SAT')

        CREATE TABLE BOOK_SAT(BOOK_HKEY UNIQUEIDENTIFIER DEFAULT NEWID(), LOAD_DATE DATETIME2(7), RECORD_SOURCE nvarchar(32), HASH_DIFF UNIQUEIDENTIFIER,
        TITLE nvarchar(256),AUTHORS nvarchar(256),ISBN10 nvarchar(10),LANGUAGE_CODE nvarchar(10),NUMBER_PAGES int,PUBLICATION_DATE DATETIME2(7),PUBLISHER nvarchar(256),
            CONSTRAINT BOOK_SAT_PK PRIMARY KEY (BOOK_HKEY, LOAD_DATE),
            CONSTRAINT BOOK_SAT_FK FOREIGN KEY(BOOK_HKEY) REFERENCES BOOK_HUB(BOOK_HKEY))

            "), connection);


                SqlCommand cmdBookBookstoreLink = new SqlCommand(string.Format(@"

        if not exists (select * from sys.tables where name='BOOK_BOOKSTORE_LINK')

        CREATE TABLE BOOK_BOOKSTORE_LINK(BOOK_BOOKSTORE_HKEY UNIQUEIDENTIFIER DEFAULT NEWID(), LOAD_DATE DATETIME2(7), RECORD_SOURCE nvarchar(32),
        BOOK_HKEY UNIQUEIDENTIFIER,  BOOKSTORE_HKEY UNIQUEIDENTIFIER,
            CONSTRAINT BOOK_BOOKSTORE_LINK_PK PRIMARY KEY (BOOK_BOOKSTORE_HKEY, LOAD_DATE))

            "), connection);

                cmdBookHub.CommandType = CommandType.Text;
                cmdBookSat.CommandType = CommandType.Text;
                //cmdBookInfoSat.CommandType = CommandType.Text;
                //cmdBookItemSat.CommandType = CommandType.Text;
                cmdBookBookstoreLink.CommandType = CommandType.Text;
                connection.Open();
                SqlTransaction sqlTran = connection.BeginTransaction();
                SqlCommand command = connection.CreateCommand();
                cmdBookHub.Transaction = sqlTran;
                cmdBookHub.ExecuteNonQuery();
                cmdBookSat.Transaction = sqlTran;
                cmdBookSat.ExecuteNonQuery();
                //cmdBookInfoSat.Transaction = sqlTran;
                //cmdBookInfoSat.ExecuteNonQuery();
                //cmdBookItemSat.Transaction = sqlTran;
                //cmdBookItemSat.ExecuteNonQuery();
                cmdBookBookstoreLink.Transaction = sqlTran;
                cmdBookBookstoreLink.ExecuteNonQuery();
                sqlTran.Commit();
            }
            
        }


        public static void CreateBookView()
        {
            var connectionString = Startup.ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {

                var cmdCreateView = new SqlCommand(string.Format(@"

        CREATE VIEW BOOK_SAT_LATEST AS 
            SELECT s.* , h.ISBN13 
                FROM BOOK_HUB h 
                JOIN BOOK_SAT s on h.BOOK_HKEY = s.BOOK_HKEY 
                JOIN 
                    (SELECT s.BOOK_HKEY, max(s.LOAD_DATE) as LOAD_DATE , h.ISBN13 
                        FROM BOOK_HUB h 
                        JOIN BOOK_SAT s on h.BOOK_HKEY = s.BOOK_HKEY
                        GROUP BY s.BOOK_HKEY , h.ISBN13 
                    )lastRecord on lastRecord.ISBN13 = h.ISBN13 and lastRecord.LOAD_DATE = s.LOAD_DATE

            "), connection);

                cmdCreateView.CommandType = CommandType.Text;
                connection.Open();
                SqlTransaction sqlTran = connection.BeginTransaction();
                SqlCommand command = connection.CreateCommand();
                cmdCreateView.Transaction = sqlTran;
                cmdCreateView.ExecuteNonQuery();
                sqlTran.Commit();
            }
        }





        //BOOKSTORE PART

        public static void CreateBookstoreTables()
        {
            string connectionString = Startup.ConnectionString;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                SqlCommand cmdBookstoreHub = new SqlCommand(string.Format(@"

            if not exists (select * from sys.tables where name='BOOKSTORE_HUB')

            CREATE TABLE BOOKSTORE_HUB
            (BOOKSTORE_HKEY UNIQUEIDENTIFIER DEFAULT NEWID(),LOAD_DATE DATETIME2(7), RECORD_SOURCE nvarchar(32),BOOKSTORE_ID int,
                CONSTRAINT BOOKSTORE_HUB_PK PRIMARY KEY (BOOKSTORE_HKEY))

            "), connection);


                SqlCommand cmdBookstoreSat = new SqlCommand(string.Format(@"

        if not exists (select * from sys.tables where name='BOOKSTORE_SAT')

        CREATE TABLE BOOKSTORE_SAT
            (BOOKSTORE_HKEY UNIQUEIDENTIFIER DEFAULT NEWID(), LOAD_DATE DATETIME2(7), RECORD_SOURCE nvarchar(32), HASH_DIFF UNIQUEIDENTIFIER, NAME nvarchar(32),
            STREET_NAME nvarchar(32),CITY nvarchar(32), COUNTRY nvarchar(32), POSTAL_CODE nvarchar(10),BANK_ACCOUNT nvarchar(11),
                CONSTRAINT BOOKSTORE_SAT_PK PRIMARY KEY (BOOKSTORE_HKEY, LOAD_DATE),
                CONSTRAINT BOOKSTORE_SAT_FK FOREIGN KEY(BOOKSTORE_HKEY) REFERENCES BOOKSTORE_HUB(BOOKSTORE_HKEY))

            "), connection);

                cmdBookstoreHub.CommandType = CommandType.Text;
                cmdBookstoreSat.CommandType = CommandType.Text;
                connection.Open();
                SqlTransaction sqlTran = connection.BeginTransaction();
                SqlCommand command = connection.CreateCommand();
                cmdBookstoreHub.Transaction = sqlTran;
                var resultHub = cmdBookstoreHub.ExecuteNonQuery();
                cmdBookstoreSat.Transaction = sqlTran;
                var resultSat = cmdBookstoreSat.ExecuteNonQuery();
                sqlTran.Commit();
            }
        }
        public static void CreateBookstoreView()
        {
            var connectionString = Startup.ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {

                var cmdCreateView = new SqlCommand(string.Format(@"

        CREATE VIEW BOOKSTORE_SAT_LATEST AS 
            SELECT s.* , h.BOOKSTORE_ID 
                FROM BOOKSTORE_HUB h 
                JOIN BOOKSTORE_SAT s on h.BOOKSTORE_HKEY = s.BOOKSTORE_HKEY 
                JOIN 
                    (SELECT s.BOOKSTORE_HKEY, max(s.LOAD_DATE) as LOAD_DATE , h.BOOKSTORE_ID 
                        FROM BOOKSTORE_HUB h 
                        JOIN BOOKSTORE_SAT s on h.BOOKSTORE_HKEY = s.BOOKSTORE_HKEY
                        GROUP BY s.BOOKSTORE_HKEY , h.BOOKSTORE_ID 
                    )lastRecord on lastRecord.BOOKSTORE_ID = h.BOOKSTORE_ID and lastRecord.LOAD_DATE = s.LOAD_DATE

            "), connection);

                cmdCreateView.CommandType = CommandType.Text;
                connection.Open();
                SqlTransaction sqlTran = connection.BeginTransaction();
                SqlCommand command = connection.CreateCommand();
                cmdCreateView.Transaction = sqlTran;
                cmdCreateView.ExecuteNonQuery();
                sqlTran.Commit();
            }
        }




        //CUSTOMER PART

        public static void CreateCustomerTables()
        {

            string connectionString = Startup.ConnectionString;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                SqlCommand cmdCustomerkHub = new SqlCommand(string.Format(@"

        if not exists (select * from sys.tables where name='CUSTOMER_HUB')

        CREATE TABLE CUSTOMER_HUB
            (CUSTOMER_HKEY UNIQUEIDENTIFIER DEFAULT NEWID(), LOAD_DATE DATETIME2(7), RECORD_SOURCE nvarchar(32),CUSTOMER_ID int,
                CONSTRAINT CUSTOMER_HUB_PK PRIMARY KEY (CUSTOMER_HKEY))
        
            "), connection);

                SqlCommand cmdCustomerSat = new SqlCommand(string.Format(@"

        if not exists (select * from sys.tables where name='CUSTOMER_SAT')

        CREATE TABLE CUSTOMER_SAT
            (CUSTOMER_HKEY UNIQUEIDENTIFIER DEFAULT NEWID(), LOAD_DATE DATETIME2(7), RECORD_SOURCE nvarchar(32), HASH_DIFF UNIQUEIDENTIFIER, NAME nvarchar(20),SURNAME nvarchar(20),
             STREET_NAME nvarchar(40),STREET_NUMBER int,CITY nvarchar(32), COUNTRY nvarchar(32), POSTAL_CODE nvarchar(10),DATE_OF_BIRTH DATETIME2(7), EMAIL nvarchar(40), BANK_ACCOUNT nvarchar(11),
                CONSTRAINT CUSTOMER_SAT_PK PRIMARY KEY (CUSTOMER_HKEY, LOAD_DATE), 
                CONSTRAINT CUSTOMER_SAT_FK FOREIGN KEY(CUSTOMER_HKEY) REFERENCES CUSTOMER_HUB(CUSTOMER_HKEY))
        
            "), connection);

                SqlCommand cmdCustomerBookstoreLink = new SqlCommand(string.Format(@"

        if not exists (select * from sys.tables where name='CUSTOMER_BOOKSTORE_LINK')

        CREATE TABLE CUSTOMER_BOOKSTORE_LINK(CUSTOMER_BOOKSTORE_HKEY UNIQUEIDENTIFIER DEFAULT NEWID(), LOAD_DATE DATETIME2(7), RECORD_SOURCE nvarchar(32),
        CUSTOMER_HKEY UNIQUEIDENTIFIER,  BOOKSTORE_HKEY UNIQUEIDENTIFIER,
            CONSTRAINT CUSTOMER_BOOKSTORE_LINK_PK PRIMARY KEY (CUSTOMER_BOOKSTORE_HKEY, LOAD_DATE))

            "), connection);

                SqlCommand cmdCustomerBookLink = new SqlCommand(string.Format(@"

        if not exists (select * from sys.tables where name='CUSTOMER_BOOK_LINK')

        CREATE TABLE CUSTOMER_BOOK_LINK(CUSTOMER_BOOK_HKEY UNIQUEIDENTIFIER DEFAULT NEWID(), LOAD_DATE DATETIME2(7), RECORD_SOURCE nvarchar(32),
        CUSTOMER_HKEY UNIQUEIDENTIFIER,  BOOK_HKEY UNIQUEIDENTIFIER,
            CONSTRAINT CUSTOMER_BOOK_LINK_PK PRIMARY KEY (CUSTOMER_BOOK_HKEY, LOAD_DATE))

            "), connection);

                cmdCustomerkHub.CommandType = CommandType.Text;
                cmdCustomerSat.CommandType = CommandType.Text;
                cmdCustomerBookstoreLink.CommandType = CommandType.Text;
                cmdCustomerBookLink.CommandType = CommandType.Text;
                connection.Open();
                SqlTransaction sqlTran = connection.BeginTransaction();
                SqlCommand command = connection.CreateCommand();
                cmdCustomerkHub.Transaction = sqlTran;
                cmdCustomerkHub.ExecuteNonQuery();
                cmdCustomerSat.Transaction = sqlTran;
                cmdCustomerSat.ExecuteNonQuery();
                cmdCustomerBookstoreLink.Transaction = sqlTran;
                cmdCustomerBookstoreLink.ExecuteNonQuery();
                cmdCustomerBookLink.Transaction = sqlTran;
                cmdCustomerBookLink.ExecuteNonQuery();
                sqlTran.Commit();
            }
        }

        public static void CreateCustomerView()
        {
            var connectionString = Startup.ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {

                var cmdCreateView = new SqlCommand(string.Format(@"

        CREATE VIEW CUSTOMER_SAT_LATEST AS 
            SELECT s.* , h.CUSTOMER_ID 
                FROM CUSTOMER_HUB h 
                JOIN CUSTOMER_SAT s on h.CUSTOMER_HKEY = s.CUSTOMER_HKEY 
                JOIN 
                    (SELECT s.CUSTOMER_HKEY, max(s.LOAD_DATE) as LOAD_DATE , h.CUSTOMER_ID 
                        FROM CUSTOMER_HUB h 
                        JOIN CUSTOMER_SAT s on h.CUSTOMER_HKEY = s.CUSTOMER_HKEY
                        GROUP BY s.CUSTOMER_HKEY , h.CUSTOMER_ID 
                    )lastRecord on lastRecord.CUSTOMER_ID = h.CUSTOMER_ID and lastRecord.LOAD_DATE = s.LOAD_DATE

            "), connection);

                cmdCreateView.CommandType = CommandType.Text;
                connection.Open();
                SqlTransaction sqlTran = connection.BeginTransaction();
                SqlCommand command = connection.CreateCommand();
                cmdCreateView.Transaction = sqlTran;
                cmdCreateView.ExecuteNonQuery();
                sqlTran.Commit();
            }
        }


        public static void CreateBookstoreBookLinkTables()
        {
            string connectionString = Startup.ConnectionString;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                var cmdLink = new SqlCommand(string.Format(@"

        if not exists (select * from sys.tables where name='BOOKSTORE_BOOK_LINK')

        CREATE TABLE BOOKSTORE_BOOK_LINK
            (BOOKSTORE_BOOK_LINK_HKEY UNIQUEIDENTIFIER DEFAULT NEWID(), LOAD_DATE DATETIME2(7), RECORD_SOURCE nvarchar(32),BOOKSTORE_HKEY UNIQUEIDENTIFIER DEFAULT NEWID(), BOOK_HKEY UNIQUEIDENTIFIER DEFAULT NEWID(),ISBN13 nvarchar(13),BOOKSTORE_ID int,
        CONSTRAINT BOOKSTORE_BOOK_LINK_PK PRIMARY KEY (BOOKSTORE_BOOK_LINK_HKEY))

            "), connection);



                SqlCommand cmdBookInfoSat = new SqlCommand(string.Format(@"

            if not exists (select * from sys.tables where name='BOOKSTORE_BOOK_LINK_SAT')

            CREATE TABLE BOOKSTORE_BOOK_LINK_SAT(BOOKSTORE_BOOK_LINK_HKEY UNIQUEIDENTIFIER DEFAULT NEWID(), LOAD_DATE DATETIME2(7), RECORD_SOURCE nvarchar(32), HASH_DIFF UNIQUEIDENTIFIER,
            COUNT int, AVAILABLE int,
                CONSTRAINT BOOKSTORE_BOOK_LINK_SAT_PK PRIMARY KEY (BOOKSTORE_BOOK_LINK_HKEY, LOAD_DATE),
                CONSTRAINT BOOKSTORE_BOOK_LINK_SAT_FK FOREIGN KEY(BOOKSTORE_BOOK_LINK_HKEY) REFERENCES BOOKSTORE_BOOK_LINK(BOOKSTORE_BOOK_LINK_HKEY, LOAD_DATE))

                "), connection);



                SqlCommand cmdBookItemSat = new SqlCommand(string.Format(@"

            if not exists (select * from sys.tables where name='BOOKSTORE_BOOK_ITEM_SAT')

            CREATE TABLE BOOKSTORE_BOOK_ITEM_SAT(BOOK_ITEM_HKEY UNIQUEIDENTIFIER DEFAULT NEWID(), LOAD_DATE DATETIME2(7), RECORD_SOURCE nvarchar(32), HASH_DIFF UNIQUEIDENTIFIER,
            BOOKSTORE_BOOK_LINK_HKEY UNIQUEIDENTIFIER,
            STATUS int, DUE_DATE DATETIME2(7),PRICE DECIMAL(10,2),
                CONSTRAINT BOOK_ITEM_SAT_PK PRIMARY KEY (BOOK_ITEM_HKEY, LOAD_DATE),
                CONSTRAINT BOOK_ITEM_SAT_FK FOREIGN KEY(BOOKSTORE_BOOK_LINK_HKEY) REFERENCES BOOKSTORE_BOOK_LINK(BOOKSTORE_BOOK_LINK_HKEY))

                "), connection);

                cmdLink.CommandType = CommandType.Text;
                cmdLink.CommandType = CommandType.Text;
                connection.Open();
                SqlTransaction sqlTran = connection.BeginTransaction();
                SqlCommand command = connection.CreateCommand();
                cmdLink.Transaction = sqlTran;
                var resultHub = cmdLink.ExecuteNonQuery();
                sqlTran.Commit();
            }

        }




        public static void CreateInvoiceTables()
        {

            string connectionString = Startup.ConnectionString;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                SqlCommand cmdCustomerkHub = new SqlCommand(string.Format(@"

        if not exists (select * from sys.tables where name='INVOICE_HUB')

        CREATE TABLE INVOICE_HUB
            (INVOICE_HKEY UNIQUEIDENTIFIER, LOAD_DATE DATETIME2(7), RECORD_SOURCE NVARCHAR(32), INVOICE_ID int identity(1,1),PRIMARY KEY(INVOICE_HKEY))

        
            "), connection);

                SqlCommand cmdCustomerSat = new SqlCommand(string.Format(@"

        if not exists (select * from sys.tables where name='INVOICE_SAT')

       
        CREATE TABLE INVOICE_SAT
            (INVOICE_HKEY UNIQUEIDENTIFIER, LOAD_DATE DATETIME2(7), RECORD_SOURCE NVARCHAR(32), HASH_DIFF UNIQUEIDENTIFIER,
            PRICE DECIMAL(10,2),CURRENCY_CODE NVARCHAR(3),ISSUE_DATE DATETIME2(7),DUE_DATE DATETIME2(7),STATUS INT, CONSTRAINT invoice_sat_pk PRIMARY KEY(INVOICE_HKEY, LOAD_DATE), 
            CONSTRAINT invoice_sat_fk FOREIGN KEY(INVOICE_HKEY) REFERENCES INVOICE_HUB(INVOICE_HKEY))
        
            "), connection);

                SqlCommand cmdCustomerBookstoreLink = new SqlCommand(string.Format(@"

        if not exists (select * from sys.tables where name='CUSTOMER_BOOKSTORE_LINK')

        CREATE TABLE CUSTOMER_BOOKSTORE_LINK(CUSTOMER_BOOKSTORE_HKEY UNIQUEIDENTIFIER DEFAULT NEWID(), LOAD_DATE DATETIME2(7), RECORD_SOURCE nvarchar(32),
        CUSTOMER_HKEY UNIQUEIDENTIFIER,  BOOKSTORE_HKEY UNIQUEIDENTIFIER,
            CONSTRAINT CUSTOMER_BOOKSTORE_LINK_PK PRIMARY KEY (CUSTOMER_BOOKSTORE_HKEY, LOAD_DATE))

            "), connection);

                SqlCommand cmdCustomerBookLink = new SqlCommand(string.Format(@"

        if not exists (select * from sys.tables where name='INVOICE_CUSTOMER_BOOK_LINK')

        
         CREATE TABLE INVOICE_CUSTOMER_BOOK_LINK
            (INVOICE_CUSTOMER_BOOK_HKEY UNIQUEIDENTIFIER DEFAULT NEWID(), LOAD_DATE DATETIME2(7), RECORD_SOURCE NVARCHAR(32),
            INVOICE_HKEY UNIQUEIDENTIFIER, CUSTOMER_HKEY UNIQUEIDENTIFIER, BOOK_HKEY UNIQUEIDENTIFIER, CONSTRAINT invoice_customer_book_pk PRIMARY KEY(INVOICE_CUSTOMER_BOOK_HKEY))

            "), connection);

                cmdCustomerkHub.CommandType = CommandType.Text;
                cmdCustomerSat.CommandType = CommandType.Text;
                cmdCustomerBookstoreLink.CommandType = CommandType.Text;
                cmdCustomerBookLink.CommandType = CommandType.Text;
                connection.Open();
                SqlTransaction sqlTran = connection.BeginTransaction();
                SqlCommand command = connection.CreateCommand();
                cmdCustomerkHub.Transaction = sqlTran;
                cmdCustomerkHub.ExecuteNonQuery();
                cmdCustomerSat.Transaction = sqlTran;
                cmdCustomerSat.ExecuteNonQuery();
                cmdCustomerBookstoreLink.Transaction = sqlTran;
                cmdCustomerBookstoreLink.ExecuteNonQuery();
                cmdCustomerBookLink.Transaction = sqlTran;
                cmdCustomerBookLink.ExecuteNonQuery();
                sqlTran.Commit();
            }
        }

        public static void CreateInvoiceView()
        {
            var connectionString = Startup.ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {

                var cmdCreateView = new SqlCommand(string.Format(@"

        CREATE VIEW INVOICE_SAT_LATEST AS 
            SELECT s.* , h.INVOICE_ID 
                FROM INVOICE_HUB h 
                JOIN INVOICE_SAT s on h.INVOICE_HKEY = s.INVOICE_HKEY 
                JOIN 
                    (SELECT s.INVOICE_HKEY, max(s.LOAD_DATE) as LOAD_DATE , h.INVOICE_ID 
                        FROM INVOICE_HUB h 
                        JOIN INVOICE_SAT s on h.INVOICE_HKEY = s.INVOICE_HKEY
                        GROUP BY s.INVOICE_HKEY , h.INVOICE_ID 
                    )lastRecord on lastRecord.INVOICE_ID = h.INVOICE_ID and lastRecord.LOAD_DATE = s.LOAD_DATE

            "), connection);

                cmdCreateView.CommandType = CommandType.Text;
                connection.Open();
                SqlTransaction sqlTran = connection.BeginTransaction();
                SqlCommand command = connection.CreateCommand();
                cmdCreateView.Transaction = sqlTran;
                cmdCreateView.ExecuteNonQuery();
                sqlTran.Commit();
            }
        }

        public static void CreateBookItemView()
        {
            var connectionString = Startup.ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {

                var cmdCreateView = new SqlCommand(string.Format(@"

     CREATE VIEW BOOKSTORE_BOOK_ITEM_SAT_LATEST AS 
            SELECT s.* 
                FROM BOOKSTORE_BOOK_ITEM_SAT s
                JOIN 
                    (SELECT s.BOOK_ITEM_HKEY, max(s.LOAD_DATE) as LOAD_DATE
                        FROM BOOKSTORE_BOOK_ITEM_SAT s
                        GROUP BY s.BOOK_ITEM_HKEY 
                    )lastRecord on lastRecord.BOOK_ITEM_HKEY = s.BOOK_ITEM_HKEY and lastRecord.LOAD_DATE = s.LOAD_DATE

            "), connection);

                cmdCreateView.CommandType = CommandType.Text;
                connection.Open();
                SqlTransaction sqlTran = connection.BeginTransaction();
                SqlCommand command = connection.CreateCommand();
                cmdCreateView.Transaction = sqlTran;
                cmdCreateView.ExecuteNonQuery();
                sqlTran.Commit();
            }
        }


    }
}
