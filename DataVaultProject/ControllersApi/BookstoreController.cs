﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataVaultProject.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace DataVaultProject.ControllersApi
{
    [Route("api/bookstore")]
    [ApiController]
    public class BookstoreController : ControllerBase
    {
        // GET: api/Bookstore
        [HttpGet]
        public string Get()
        {
            var bookstoreService = new BookstoreService();
            var bookstores = bookstoreService.getAllBookstores();
            return JsonConvert.SerializeObject(bookstores);
        }

        //// GET: api/Bookstore/5
        //[HttpGet("{id}", Name = "GetBookstore")]
        //public string Get(int id)
        //{
        //    return id.ToString();
        //}

        //// POST: api/Bookstore
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT: api/Bookstore/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
