using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NLog;
using LoggerService;
using DataVaultProject.EtlServices;
using Nest;
using Microsoft.EntityFrameworkCore;

namespace DataVaultProject
{
    public class Startup
    {
        public static string ConnectionString { get; set; }
        public Startup(IConfiguration configuration)
        {
            LogManager.LoadConfiguration(String.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));
            Configuration = configuration;
            Configuration = new ConfigurationBuilder()
               .AddJsonFile("appSettings.json") // Get Connectionstring from appsetting.json
               .Build();
            ConnectionString = Configuration["ConnectionStrings:ConnectionString"];

            //Create Database

            //DatabaseContext.CreateBookTables();
            //DatabaseContext.CreateBookView();
            //DatabaseContext.CreateBookstoreTables();
            //DatabaseContext.CreateBookstoreView();
            //DatabaseContext.CreateCustomerTables();
            //DatabaseContext.CreateCustomerView();
            //DatabaseContext.CreateBookstoreBookLinkTables();


            //Import data
            //Import import = new Import();
            //import.ImportBooks();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddDbContextPool<Models.AppDbContext>(options => options.UseSqlServer(ConnectionString));
            //services.AddHostedService<BackgroundEtlService>();
            //services.AddSingleton<IConfiguration>(Configuration);
            //Added logging service
            services.AddSingleton<ILoggerManager, LoggerManager>();

            services.AddMvc();
            services.AddSwaggerGen(c => c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "My DataVault2.0 API", Version = "v1" }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json","My DataVault2.0 API");
            });
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }


    }
}
