﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace DataVaultProject.EtlServices
{
    public class Import
    {
		
		public void ImportBooks()

        {
            DataTable csvData = new DataTable();
            //string startupPath = System.IO.Directory.GetCurrentDirectory();
            //string startupPath2 = Environment.CurrentDirectory;
            using (TextFieldParser csvReader = new TextFieldParser("Resources\\books2.csv"))
            {
                csvReader.SetDelimiters(new string[] { ";" });
                csvReader.HasFieldsEnclosedInQuotes = true;
                string[] colFields = csvReader.ReadFields();
                foreach (string column in colFields)
                {
                    DataColumn datecolumn = new DataColumn(column);
                    datecolumn.AllowDBNull = true;
                    csvData.Columns.Add(datecolumn);
                }

                while (!csvReader.EndOfData)
                {
                    string[] fieldData = csvReader.ReadFields();
                    //Making empty value as null
                    for (int i = 0; i < fieldData.Length; i++)
                    {
                        if (fieldData[i] == "")
                        {
                            fieldData[i] = null;
                        }
						string[] columns = fieldData[i].Split(",");
                        
                            
						string connectionString = "Data Source=LAPTOP-H1TDLTRQ\\SQLEXPRESS;Initial Catalog=TEST;Integrated Security=True";
						using (SqlConnection connection = new SqlConnection(connectionString))
						{


							System.Guid uniqueId = Guid.NewGuid();
							foreach (DataRow row in csvData.Rows)
							{

							}
							SqlCommand commandHub = new SqlCommand("USE TEST INSERT INTO BOOK_HUB(BOOK_HKEY,LOAD_DATE,RECORD_SOURCE,ISBN13) VALUES(@uniqueId,getDate(),'Admin',@isbn)", connection);
							commandHub.CommandType = CommandType.Text;
							commandHub.Parameters.AddWithValue("@uniqueId", uniqueId);
							commandHub.Parameters.AddWithValue("@isbn", columns[5]);


							SqlCommand commandSat = new SqlCommand("USE TEST INSERT INTO BOOK_SAT(BOOK_HKEY,LOAD_DATE,RECORD_SOURCE,TITLE,AUTHORS,ISBN10,LANGUAGE_CODE,NUMBER_PAGES,PUBLICATION_DATE,PUBLISHER) VALUES(@uniqueId,getDate(), @RecordSource,@Title,@Authors,@Isbn,@LanguageCode, @NumberPages, @PublicationDate, @Publisher)", connection);
							commandSat.CommandType = CommandType.Text;
							commandSat.Parameters.AddWithValue("@uniqueId", uniqueId);
							commandSat.Parameters.AddWithValue("@RecordSource", "Aleksandar");
							commandSat.Parameters.AddWithValue("@Title", Truncate(columns[1],256));
							commandSat.Parameters.AddWithValue("@Authors", Truncate(columns[2],256));
							commandSat.Parameters.AddWithValue("@Isbn", columns[4]);
							commandSat.Parameters.AddWithValue("@LanguageCode", columns[6]);
							commandSat.Parameters.AddWithValue("@NumberPages", Int32.Parse(columns[7]));
							string[] publicationDate = columns[10].Split("/");
							
							commandSat.Parameters.AddWithValue("@PublicationDate", publicationDate[2]+"-"+publicationDate[0]+"-"+publicationDate[1]);
							commandSat.Parameters.AddWithValue("@Publisher", Truncate(columns[11],256));
							
							connection.Open();
							SqlTransaction sqlTran = connection.BeginTransaction();

							commandHub.Transaction = sqlTran;
							commandHub.ExecuteNonQuery();

							commandSat.Transaction = sqlTran;
							var result = commandSat.ExecuteNonQuery();

							sqlTran.Commit();
						}

						}
                  
                    csvData.Rows.Add(fieldData);
                }
            }
		}

		public string Truncate(string value, int maxLength)
		{
			if (string.IsNullOrEmpty(value)) { return value; }

			return value.Substring(0, Math.Min(value.Length, maxLength));
		}

	}
}
