﻿using DataVaultProject.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace DataVaultProject.Encription.HashDiff
{
    public static class HashDiff
    {

		public static string GetHashDiff(IFormCollection collection)
		{
			return GetHash(GetStringRepresentation(collection));
		}



		public static string GetHashDiff(Bookstore collection)
		{
			return GetHash(GetStringRepresentation(collection));
		}

		private static string GetStringRepresentation(Bookstore bookstore)
		{
			string concatenatedKeys = "";
			foreach (PropertyInfo prop in bookstore.GetType().GetProperties())
			{
				var value = prop.GetValue(bookstore);
				concatenatedKeys += value.ToString();
			}
			return concatenatedKeys;
		}




		public static string GetHashDiff(Book collection)
		{
			return GetHash(GetStringRepresentation(collection));
		}

		private static string GetStringRepresentation(Book book)
		{
			string concatenatedKeys = "";
			foreach (PropertyInfo prop in book.GetType().GetProperties())
			{
				var value = prop.GetValue(book);
				concatenatedKeys += value.ToString();
			}
			return concatenatedKeys;
		}







		private static string GetStringRepresentation(IFormCollection collection)
		{
			string concatenatedKeys = "";
			foreach (var key in collection.Keys)
			{
				if (key != "__RequestVerificationToken")
				{
					var value = collection[key];
					concatenatedKeys += value.ToString();
				}
			}
			return concatenatedKeys;
		}
		private static string GetHash(string toHash)
		{
			if (String.IsNullOrEmpty(toHash)) return null;
			using (var md5 = MD5.Create())
			{
				var inputBytes = Encoding.UTF8.GetBytes(toHash);
				var bytes = md5.ComputeHash(inputBytes);

				var sb = new StringBuilder();

				foreach (var b in bytes)
				{
					sb.Append(b.ToString("X2"));
				}

				return sb.ToString();
			}
		}
		//public string ComputeHash()
		//{
		//	string inputString = GetStringRepresentation();
		//	if (String.IsNullOrEmpty(inputString)) return null;
		//	Byte[] input = Encoding.UTF8.GetBytes(inputString);
		//	using (SHA256 sha = SHA256Managed.Create())
		//	{
		//		byte[] hashedOutput = sha.ComputeHash(input);
		//		// Create a new Stringbuilder to collect the bytes and create a string
		//		StringBuilder sBuilder = new StringBuilder();
		//		// Loop through each byte of the hashed data and format each one as a hexadecimal string
		//		for (int i = 0; i < hashedOutput.Length; i++)
		//		{
		//			sBuilder.Append(hashedOutput[i].ToString("x2"));
		//		}
		//		// Return the hexadecimal string.
		//		return sBuilder.ToString();
		//	}
		//}
		//public virtual string GetStringRepresentation()
		//{
		//	// get attributes hash
		//	string attributesHash = String.Empty;
		//	if (AttributeValueSet != null)
		//	{
		//		attributesHash = AttributeValueSet.ComputeHash();
		//	}

		//	// create string representation
		//	return string.Join(";", String.Empty,
		//							BalanceAccountId,
		//							Type,
		//							DocumentTypeId,
		//							DocumentNo,
		//							CancelDocumentNo,
		//							ProposedPostingDate.Date.ToString("yyyyMMdd"),
		//							AmountBeforeTax.ToString("0.00", CultureInfo.InvariantCulture),
		//							TaxAmount.ToString("0.00", CultureInfo.InvariantCulture),
		//							GrossAmount.ToString("0.00", CultureInfo.InvariantCulture),
		//							Description,
		//							attributesHash,
		//							businessEventId).TrimEnd(';');
		//}
	}
}