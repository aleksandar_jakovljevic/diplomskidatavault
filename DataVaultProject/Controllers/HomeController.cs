﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DataVaultProject.Models;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using LoggerService;
using MongoDB.Driver;
using System.Configuration;
using DataVaultProject.Services;
using Microsoft.Extensions.Configuration;

namespace DataVaultProject.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILoggerManager _logger;

   

        public HomeController(ILoggerManager logger)
        {
            _logger = logger;

        }

        public IActionResult DataVaultModel()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public ActionResult Index()
        {
           
            return View();
        }
    }
}
