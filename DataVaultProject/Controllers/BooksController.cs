﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DataVaultProject.Models;
using DataVaultProject.Services;
using Microsoft.AspNetCore.Http;

namespace DataVaultProject.Controllers
{
    public class BooksController : Controller
    {
        private readonly AppDbContext _context;
        private BookService bookService;
        private BookstoreService bookstoreService;

        public BooksController(AppDbContext context)
        {
            _context = context;
            bookService = new BookService();
            bookstoreService = new BookstoreService();
        }

        // GET: Books
        public async Task<IActionResult> Index()
        {
            //SsisExecution.ExecuteBookPackage();
            //return View(await _context.Books.ToListAsync());
            return View(bookService.GetAll());
        }

        // GET: Books/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = bookService.GetBook(id);
                //await _context.Books
                //.FirstOrDefaultAsync(m => m.Isbn13 == id);
            if (book == null)
            {
                return NotFound();
            }

            return View(book);
        }

        // GET: Book/Search
        public ActionResult Search() 
        {
            return View();
        }

        // POST: Book/Search
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Search(IFormCollection collection)
        {
            try
            {
                List<Book> books = bookService.GetAllFilter(collection);

                return View("Index", books);
            }
            catch
            {
                return View();
            }
        }

        // GET: Books/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Books/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Title,Authors,AverageRating,Isbn10,Isbn13,LanguageCode,NumPages,RatingsCount,TextReviewCount,PublicationDate,Publisher")] Book book)
        {
            if (ModelState.IsValid)
            {
                _context.Add(book);
                await _context.SaveChangesAsync();
                //bookService.AddBook(book);
                SsisExecution.ExecuteBookPackage();
                return RedirectToAction(nameof(Index));
            }
            return View(book);
        }

  


        //IMPORTING

        // GET: Book/Import
        public async Task<IActionResult> Import(string id) //Service added
        {
            ViewBag.bookstores = bookstoreService.getAllBookstores();
            Book book = bookService.GetBook(id);
            Import import = new Models.Import() ;
            import.Isbn13 = book.Isbn13;
            import.Title = book.Title;
            return View(import);
        }

        // POST: Book/Import
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Import(string id, string bookstoreId, [Bind("ImportId,Title,Isbn13,BookstoreId,Count,Available,Price,Lend,Sell")] Import import) //Service added
        {
            import.Available = import.Count;
            try
            {
                //bookService.ImportBook(id, bookstoreId, import);
                if (ModelState.IsValid)
                {
                    _context.Add(import);
                    await _context.SaveChangesAsync();
                    SsisExecution.ExecuteImportPackage();
                    return RedirectToAction(nameof(Index));
                }
                //return View(import);
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                return View();
            }
        }

        /* TRIGGER SQL
         * 
         * 
         * CREATE TRIGGER BookItemInsertTrigger ON dbo.BOOKSTORE_BOOK_LINK_SAT
FOR INSERT
AS 
declare @loopEnd int;  
declare @counter int;  
declare @bookstoreBookHkey UNIQUEIDENTIFIER;  
select @loopEnd = i.COUNT from inserted i;  
select @counter = 0;  
select @bookstoreBookHkey = i.BOOKSTORE_BOOK_LINK_HKEY from inserted i; 
WHILE(@counter<@loopEnd)
	BEGIN  
		SET @counter = @counter + 1;
	  INSERT INTO dbo.BOOKSTORE_BOOK_ITEM_SAT (BOOK_ITEM_HKEY,LOAD_DATE,RECORD_SOURCE,HASH_DIFF,BOOKSTORE_BOOK_LINK_HKEY,STATUS,DUE_DATE)
		VALUES (NEWID(),GETDATE(),'Administrator',NEWID(),@bookstoreBookHkey,1,null)
	END   

        */


        //ORDERING

        // GET: Book/Import
        public async Task<IActionResult> Order(string id) //Service added
        {
            ViewBag.bookstores = bookstoreService.getAllBookstores();
            Book book = bookService.GetBook(id);//CHECK IF THERE IS AVAILABLE BOOK


            //            select* from dbo.BOOKSTORE_BOOK_LINK l
            //join dbo.BOOKSTORE_BOOK_LINK_SAT s on s.BOOKSTORE_BOOK_LINK_HKEY = l.BOOKSTORE_BOOK_LINK_HKEY
            //join
            //(select top 1 * from dbo.BOOKSTORE_BOOK_ITEM_SAT i where i.STATUS = 1) i on i.BOOKSTORE_BOOK_LINK_HKEY = l.BOOKSTORE_BOOK_LINK_HKEY

            //where l.ISBN13 = '1234567890123'
            //and s.AVAILABLE > 0

            ViewBag.OrderStatus = 0;
            Order order = new Models.Order();
            order.Isbn13 = book.Isbn13;
            order.BookName = book.Title;
            return View(order);
        }

        // POST: Book/Import
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Order(string id, string bookstoreId, [Bind("OrderId,Isbn13,BookName,BookstoreName,CustomerId,BookstoreId,Status")] Order order) //Service added
        {
            string bookItemHkey;
            string price;
            string bookHkey = bookstoreService.GetBookIfAvailable(id, bookstoreId, out bookItemHkey, out price);
         
         
            try
            {
                if (bookHkey != "")
                {
                    ViewBag.OrderStatus = 1;
                    order.BookHkey = new Guid(bookHkey);
                    order.BookItemHkey = new Guid(bookItemHkey);
                    order.Price = Convert.ToDecimal(price);
                    if (ModelState.IsValid)
                    {
                        _context.Add(order);
                        await _context.SaveChangesAsync();
                        SsisExecution.ExecuteOrderPackage();
                        return RedirectToAction(nameof(Index));
                    }
                 
                }
                else
                {
                    ViewBag.OrderStatus = 2;
                }

                //return View(import);
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                return View();
            }
        }

        //// GET: Books/Edit/5
        //public async Task<IActionResult> Edit(string id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    //var book = await _context.Books.FindAsync(id);
        //    var book = bookService.GetBook(id);
        //    if (book == null)
        //    {
        //        return NotFound();
        //    }
        //    return View(book);
        //}

        //// POST: Books/Edit/5
        //// To protect from overposting attacks, enable the specific properties you want to bind to, for 
        //// more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(string id, [Bind("Title,Authors,AverageRating,Isbn10,Isbn13,LanguageCode,NumPages,RatingsCount,TextReviewCount,PublicationDate,Publisher")] Book book)
        //{
        //    if (id != book.Isbn13)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(book);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!BookExists(book.Isbn13))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View(book);
        //}

        // GET: Book/Edit/5
        public ActionResult Edit(string id)//Service added
        {
            return View(bookService.GetBook(id));
        }

        // POST: Book/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string id, IFormCollection collection)//Service added
        {
            try
            {
                bookService.UpdateBook(id, collection);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                return View();
            }
        }

        // GET: Books/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Books
                .FirstOrDefaultAsync(m => m.Isbn13 == id);
            if (book == null)
            {
                return NotFound();
            }

            return View(book);
        }

        // POST: Books/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var book = await _context.Books.FindAsync(id);
            _context.Books.Remove(book);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BookExists(string id)
        {
            return _context.Books.Any(e => e.Isbn13 == id);
        }
    }
}
