﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DataVaultProject.Models;

namespace DataVaultProject.Controllers
{
    public class ImportsController : Controller
    {
        private readonly AppDbContext _context;

        public ImportsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: Imports
        public async Task<IActionResult> Index()
        {
            return View(await _context.Imports.ToListAsync());
        }

        // GET: Imports/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var import = await _context.Imports
                .FirstOrDefaultAsync(m => m.ImportId == id);
            if (import == null)
            {
                return NotFound();
            }

            return View(import);
        }

        // GET: Imports/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Imports/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ImportId,Isbn13,Title,BookstoreId,Count,Available,Lend,Sell")] Import import)
        {
            if (ModelState.IsValid)
            {
                _context.Add(import);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(import);
        }

        // GET: Imports/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var import = await _context.Imports.FindAsync(id);
            if (import == null)
            {
                return NotFound();
            }
            return View(import);
        }

        // POST: Imports/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ImportId,Isbn13,Title,BookstoreId,Count,Available,Lend,Sell")] Import import)
        {
            if (id != import.ImportId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(import);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ImportExists(import.ImportId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(import);
        }

        // GET: Imports/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var import = await _context.Imports
                .FirstOrDefaultAsync(m => m.ImportId == id);
            if (import == null)
            {
                return NotFound();
            }

            return View(import);
        }

        // POST: Imports/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var import = await _context.Imports.FindAsync(id);
            _context.Imports.Remove(import);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ImportExists(int id)
        {
            return _context.Imports.Any(e => e.ImportId == id);
        }
    }
}
