﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DataVaultProject.Models;
using DataVaultProject.Services;
using Microsoft.AspNetCore.Http;

namespace DataVaultProject.Controllers
{
    public class BookstoresController : Controller
    {
        private readonly AppDbContext _context;
        private BookstoreService bookstoreService;

        public BookstoresController(AppDbContext context)
        {
            _context = context;
            bookstoreService = new BookstoreService();
        }

        // GET: Bookstores
        public async Task<IActionResult> Index()
        {
            //return View(await _context.Bookstores.ToListAsync());
            return View(bookstoreService.GetAll());
        }

        // GET: Bookstores/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var bookstore = bookstoreService.GetBookstore(id);
            //var bookstore = await _context.Bookstores
            //    .FirstOrDefaultAsync(m => m.BookstoreId == id);

            if (bookstore == null)
            {
                return NotFound();
            }

            return View(bookstore);
        }

        // GET: Bookstores/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Bookstores/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("BookstoreId,Name,Street,City,Country,PostalCode,BankAccount")] Bookstore bookstore)
        {
            if (ModelState.IsValid)
            {
                _context.Add(bookstore);
                await _context.SaveChangesAsync();
                SsisExecution.ExecuteBookstorePackage();//calling SQL Agent to start package job
                return RedirectToAction(nameof(Index));
            }
            return View(bookstore);
        }

        // GET: Bookstore/Edit/5
        public ActionResult Edit(int id)
        {
            return View(bookstoreService.GetBookstore(id));
        }

        // POST: Bookstore/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                bookstoreService.UpdateBookstore(id, collection);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        //// GET: Bookstores/Edit/5
        //public async Task<IActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var bookstore = await _context.Bookstores.FindAsync(id);
        //    if (bookstore == null)
        //    {
        //        return NotFound();
        //    }
        //    return View(bookstore);
        //}

        //// POST: Bookstores/Edit/5
        //// To protect from overposting attacks, enable the specific properties you want to bind to, for 
        //// more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("BookstoreId,Name,Street,City,Country,PostalCode,BankAccount")] Bookstore bookstore)
        //{
        //    if (id != bookstore.BookstoreId)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(bookstore);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!BookstoreExists(bookstore.BookstoreId))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View(bookstore);
        //}

        // GET: Bookstores/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var bookstore = await _context.Bookstores
        //        .FirstOrDefaultAsync(m => m.BookstoreId == id);
        //    if (bookstore == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(bookstore);
        //}

        //// POST: Bookstores/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var bookstore = await _context.Bookstores.FindAsync(id);
        //    _context.Bookstores.Remove(bookstore);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}

        //private bool BookstoreExists(int id)
        //{
        //    return _context.Bookstores.Any(e => e.BookstoreId == id);
        //}
    }
}
